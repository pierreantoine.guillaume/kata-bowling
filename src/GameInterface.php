<?php

declare(strict_types=1);

namespace Bowling;

interface GameInterface
{
    public function roll(int $pins): void;
    public function score(): int;
}
